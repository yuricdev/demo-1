import { all, call, put, takeLatest } from 'redux-saga/effects'
import { getListPostsFailure, getListPostsSuccess } from './actions';
import { getPostData } from './apiSaga'

function* getListPostsSaga(action: any): any {
    try {
        const response = yield call(getPostData);
        yield put(getListPostsSuccess({
            todosSaga: response.data
        }))
    } catch (e: any) {
        yield put(
      getListPostsFailure({
        error: e.message,
      })
    );
    }
}
function* postsSaga() {
    yield all([takeLatest('FETCH_TODO_REQUEST', getListPostsSaga)])
}

export default postsSaga;

