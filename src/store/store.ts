import { applyMiddleware, compose, createStore } from "redux";
import rootReducer from "./rootReducer";
import createSagaMiddleware from 'redux-saga'
import {persistStore, persistReducer} from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import { rootSaga } from "./rootSaga";
// import { helloSaga } from "./postsSaga";
import { composeWithDevTools } from 'redux-devtools-extension';
const sagaMiddleware = createSagaMiddleware()
const persistConfig = {
    key: 'root',
    storage: storage,
}

// const persistedReducer = persistReducer(persistConfig, rootReducer)
// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__?.({ latency: 0 }) ?? compose;

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)))

export default store;
export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga)