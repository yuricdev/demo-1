export const getListTodos = (data: any):any => {
    return {
        type: 'ADD_TODO',
        payload: data
    }
}

export const deleteTodo = (data: any):any => {
    return {
        type: 'DELETE_TODO',
        payload: data
    }
}

export const fetchTodoRequest  = (): any => ({
   type: 'FETCH_TODO_REQUEST',
})
export const getListPostsSuccess = (data: any): any => {
    return {
        type: 'FETCH_TODO_SUCCESS',
        payload: data.todosSaga
        
    }
    
}

export const getListPostsFailure = (data: any): any => {
    return {
        type: 'FETCH_TODO_FAILURE',
        payload: data
    }
}