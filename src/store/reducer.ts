const initialState: any = {
    list: [],
    listSaga: [],
}
 

const reducer = (state: any = initialState, action: any): any => {
    switch (action.type) {
        case 'ADD_TODO': {
            const newList = [...state.list]
            newList.push(action.payload)
            return {
                ...state,
                list : newList
            }
        }
            
        case 'DELETE_TODO': {
            const newList = [...state.list]
          let afterDelete =  newList.filter((todo, index) => {
                return todo !== action.payload
            })
            return {
                ...state,
                list : afterDelete
            }
        }
            
        case 'FETCH_TODO_SUCCESS': {
                const newListSaga = [...state.list]
            let arr = action.payload.map((item: any) => { return item.name })
            let newList = newListSaga.concat(arr)
            // var result = newList.filter(e => !arr.find((a: any ) => a === e));
            
            
            return {
                ...state,
                list: newList
                
                
            }
        }
            case 'FETCH_TODO_FAILURE': {
                
            return {
                ...state,
                
            }
        }
            
        default:
            return state
       
    }
}
export default reducer