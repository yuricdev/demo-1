import React, { Component } from 'react'
// import IPage from '../interfaces/page'
import { Button, Form, Input, Space } from 'antd';
import { connect } from 'react-redux';
import * as actions from '../store/actions';
import style from './style.module.scss'
class ClassTodos extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            listTodos: [],
            inputTodos: '',
        }
    }
    componentDidMount() {
        // this.props.getListTodosRedux()
        // this.props.fetchTodoRequestRedux()
    
    }

    componentDidUpdate(prevProps: any, prevState: any) {
        // this.props.getListTodosRedux()
        if (prevProps.todosListRedux !== this.props.todosListRedux) {
            let arrTodos = this.props.todosListRedux;
            
                 this.setState({
                listTodos: arrTodos 
            })
            
           
        }
    }
     
    handleChange = (e: any) => {
        this.setState({
            inputTodos: e.target.value
        })
    }
    handleClick =  (e: any) => {
        e.preventDefault();
        this.props.getListTodosRedux(this.state.inputTodos)
        this.setState({
            inputTodos: ''
        })
        
    }

    handleDelete = (data: any) => {
        this.props.deleteTodoRedux(data)
    }

    render() {
        let {listTodos, inputTodos} = this.state
        return (
            <div style={{ textAlign: 'center', fontSize: 18, marginTop: 20 }}>
                <Form
                    layout="vertical"
                    
                >
                    <Form.Item
                        rules={[{ required: true }, { type: 'url', warningOnly: true }, { type: 'string', min: 6 }]}
                    >
                        <Input placeholder="Input class todos..."
                            size="large"
                            style={{ width: 400 }}
                            onChange={(e) => this.handleChange(e)}
                            value={inputTodos}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Space>
                            <Button type="primary" htmlType="submit"
                            onClick={this.handleClick}
                            >
                                Click Add
                            </Button>
                        </Space>
                    </Form.Item>
                </Form>
                <ul className={style.list} >

                    {listTodos && listTodos.length > 0
                        && listTodos.map((item: any, index: any) => {
                    return (
                        <li key={index}>{index}        {item} <span  onClick={() => this.handleDelete(item)}>X</span></li>
                    )
                })}
                    </ul>
            </div>
        )


    }
}

const mapStateToProps = (state: any) => {
    return {
        todosListRedux: state.reduce.list
    };
};

const mapDispatchToProps = (dispatch: any) => {
  return {
    getListTodosRedux: (data: any) => dispatch(actions.getListTodos(data)),
    deleteTodoRedux: (data: any) => dispatch(actions.deleteTodo(data)),
    fetchTodoRequestRedux: () => dispatch(actions.fetchTodoRequest())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ClassTodos);