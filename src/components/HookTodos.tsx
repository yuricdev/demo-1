import { Button, Form, Input, Space } from 'antd';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { deleteTodo, fetchTodoRequest, getListTodos } from '../store/actions';
import style from './style.module.scss'

// import IPage from '../interfaces/page'
const HookTodos = () => {
    const [inputTodos, setInputTodos] = useState('')

    const listFromRedux = useSelector((state: any)=> state.reduce.list)
    const dispatch = useDispatch()

    const handleClick = (e: any): void => {
        e.preventDefault()
        if(inputTodos !== ''){
            dispatch(getListTodos(inputTodos))
            setInputTodos(''); 
        }
    }
    
     const handleDelete = (data: any): any=> {
         dispatch(deleteTodo(data))
    }

    useEffect(() => {
        dispatch(fetchTodoRequest())
    }, [])
    
    return (
        <div style={{ textAlign: 'center', fontSize: 18, marginTop: 20 }}>
            <Form
                layout="vertical"
            >
                <Form.Item
                    rules={[{ required: true }, { type: 'url', warningOnly: true }, { type: 'string', min: 6 }]}
                >
                    <Input placeholder="Input hook todos..."
                        size="large"
                        style={{ width: 400 }}
                        onChange={e => setInputTodos(e.target.value)}
                        value={inputTodos}
                    />
                </Form.Item>
                <Form.Item>
                    <Space>
                        <Button type="primary" htmlType="submit"
                            onClick={handleClick}
                        >
                            Click Add
                        </Button>
                    </Space>
                </Form.Item>
            </Form>

            <ul className={style.list}>
                {listFromRedux && listFromRedux.length > 0
                    && listFromRedux.map((item: any, index:any) => {
                    return (
                        
                        <li key={index}>{index}   {item} <span  onClick={() => handleDelete(item)}>X</span> </li>
                        
                    )
                })}
                    </ul>
        </div>
    )
}
    
export default HookTodos;