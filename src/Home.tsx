import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, Outlet, NavLink } from "react-router-dom";
import ClassTodos from './components/ClassTodos';
import HookTodos from './components/HookTodos';
import { Layout, Menu, Breadcrumb,   } from 'antd';
// import routes from './config/routes'
// import './Home.css';
import Sider from 'antd/lib/layout/Sider';
import SubMenu from 'antd/lib/menu/SubMenu';
const { Header, Content, Footer } = Layout;

class Home extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
    }
  }

    render() {
        return (
          <>
            <Outlet />
          </>
          
        )
    }
}

export default Home;