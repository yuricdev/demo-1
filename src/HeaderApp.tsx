import React, { Component, Fragment } from 'react';
import { BrowserRouter as Router, Route, Link, Outlet, NavLink } from "react-router-dom";
import ClassTodos from './components/ClassTodos';
import HookTodos from './components/HookTodos';
import { Layout, Menu, Breadcrumb,   } from 'antd';
// import routes from './config/routes'
import './HeaderApp.css';
import Sider from 'antd/lib/layout/Sider';
import SubMenu from 'antd/lib/menu/SubMenu';
const { Header, Content, Footer } = Layout;

class App extends Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      collapsed: false,
    }
  }
 handleClick = ((e:any) => {
    console.log('click ', e);
  })
    
 
    
    render() {
        return (
          <>
      <Menu
        onClick={this.handleClick}
        style={{ width: 'fullwidth'}}
        defaultSelectedKeys={['1']}
        defaultOpenKeys={['sub1']}
        mode="inline"
      >
        <SubMenu key="sub1"  title="Navigation with Yuric">
          <Menu.ItemGroup key="g1" title="Header">
                  <Menu.Item key="1">
                    <NavLink
                      className={isActive =>
                          "nav-link" + (!isActive ? " unselected" : "")
                        }
                      to="/class-todos">
                      Class-Component
                    </NavLink>
                  </Menu.Item>
                  <Menu.Item key="2">
                    <NavLink
                      className={isActive =>
                          "nav-link" + (!isActive ? " unselected" : "")
                        }
                      to="/hook-todos">
                      Hook-Component
                    </NavLink>
                  </Menu.Item>
          </Menu.ItemGroup>
              </SubMenu> 
              
            </Menu>
            <Outlet />
          </>
          
        )
    }
}

export default App;