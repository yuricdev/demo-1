import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './HeaderApp';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ClassTodos from './components/ClassTodos';
import HookTodos from './components/HookTodos';

import {Provider} from 'react-redux';
import store, { persistor } from './store/store'
import { PersistGate } from 'redux-persist/integration/react';
import HeaderApp from './HeaderApp';
import Home from './Home';
const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

  

root.render(
  <Provider store={store}>
    {/* <PersistGate loading={null} persistor={persistor}> */}
  <React.StrictMode>
      <BrowserRouter>
        <HeaderApp />
      <Routes>
        <Route path="/" element={<Home />}>
        
        <Route path='/class-todos'
          element={<ClassTodos />}
        />
        <Route path='/hook-todos'
          element={<HookTodos />}
          />  
          </Route>
    </Routes>
    </BrowserRouter>
    
      </React.StrictMode>
      {/* </PersistGate> */}
    </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
